" let configDir=$HOME/.config/nvim/vim-configs
so $HOME/.config/nvim/config/sets.vim
so $HOME/.config/nvim/config/plugins.vim
so $HOME/.config/nvim/config/maps.vim
so $HOME/.config/nvim/config/theme.vim

" for the background version (dark or light)
set background=dark

" Load Plugins Configuration
so $HOME/.config/nvim/config/plugins/airline.vim
so $HOME/.config/nvim/config/plugins/coc.vim
so $HOME/.config/nvim/config/plugins/indentlines.vim
so $HOME/.config/nvim/config/plugins/nerdCommenter.vim
so $HOME/.config/nvim/config/plugins/nerdtree.vim
so $HOME/.config/nvim/config/plugins/rainbowParentheses.vim
so $HOME/.config/nvim/config/plugins/signify.vim
