# Archivos Neovim

Archivos de configuración para utilizar NeoVim como editor de código.

Este repositorio esta hecho como complemento del vídeo tutorial [Neovim Desde Cero](https://www.youtube.com/watch?v=32S-X-D_W-k) que fue el proyecto desarrollado para el GLUD.

## Dependencias

Algunos plugins tienen dependencias, en el caso de "neoclide/coc.nvim" se necesita nodejs.

### Neovim

'''
sudo apt install nodejs npm python2 python3 pip

pip install pynvim
npm install -g neovim clangd
'''

## Para usar esta configuración

- Clonar el repositorio

	`git clone https://gitlab.com/afmartinr/archivos-neovim.git ~/.confg/nvim`

Luego, al ingresar a NeoVim, se instalaran los plugins que no necesitan otras dependencias.
