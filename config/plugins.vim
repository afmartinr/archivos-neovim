" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
						\| PlugInstall --sync | source $MYVIMRC
						\| endif

" Call to the folder that will save the plugins
call plug#begin('$HOME/.config/neovimplugins')

" Syntax
Plug 'sheerun/vim-polyglot'

" Status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Themes
Plug 'rakr/vim-one'


" Typing
Plug 'jiangmiao/auto-pairs'
Plug 'alvan/vim-closetag'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
"Plug 'godlygeek/tabular'
Plug 'terryma/vim-multiple-cursors'


" Autocomplete
Plug 'honza/vim-snippets'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" IDE
" 	Three
Plug 'scrooloose/nerdtree'
Plug 'editorconfig/editorconfig-vim'                                 " Configuraciones globales
" MD
"Plug 'MichaelMure/mdr'
"Plug 'skanehira/preview-markdown.vim', {'for': 'markdown'}

"Git integration
Plug 'mhinz/vim-signify'

" Code Display
Plug 'yggdroot/indentline'
Plug 'lilydjwg/colorizer'
Plug 'luochen1990/rainbow'

" Interface
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'PhilRunninger/nerdtree-buffer-ops'
Plug 'ryanoasis/vim-devicons'
"Plug 'junegunn/goyo.vim'
Plug 'tpope/vim-repeat'

" HTML
Plug 'mattn/emmet-vim'
call plug#end()
