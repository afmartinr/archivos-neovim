let mapleader=" "

inoremap <silent>ii <Esc>
nnoremap <Leader>w :w<CR>
"nnoremap <Leader>q :q<CR> " No dejemos que salir sea tan facíl
nmap <C-S> :write<CR>

" move into diferent splits
nmap <C-H> <C-W>h
nmap <C-J> <C-W>j
nmap <C-K> <C-W>k
nmap <C-L> <C-W>l

" width split resize
nnoremap <Leader>+ 10<C-w>>
nnoremap <Leader>- 10<C-w><

" quick semi
nnoremap <Leader>; $a;<Esc>

map <Leader>p :Files<CR>

" buffers
map <Leader>ob :Buffers<CR>
map <Leader>d :bdelete<CR>
map <Leader>h :bprevious<CR>
"nmap <C-STab>h :bprevious<CR>
map <Leader>l :bnext<CR>
"nmap <C-TAB> :bnext<CR>
