set encoding=utf-8
set mouse=a  
set clipboard=unnamedplus

" Save changes 
set undodir=~/.undodir/
set undofile

" Tabs
set sw=4        " tabs in # spaces
set noexpandtab " No separar tabs
set smartindent " Indent like the last line

" Cursorline and a column
set cursorline
set colorcolumn=81
"highlight ColorColumn ctermbg=0 guibg=lightgrey

" Numbers
set number
set relativenumber
"set numberwidth=1

set showcmd " Show commands
set ruler " Current cursor position 
"set showmatch

"" Searching
set hlsearch    " highlight matches
set incsearch   " incremental searching
set ignorecase  " searches are case insensitive...
set smartcase   " ... unless they contain at least one capital letter

"" Split
set splitbelow
set splitright

" if hidden is not set, TextEdit might fail.
set hidden

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=1

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
"set shortmess+=c

" always show signcolumns
"set signcolumn=yes

"let g:loaded_perl_provider = 0
let g:loaded_ruby_provider = 0
