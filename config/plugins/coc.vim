let g:coc_disable_startup_warning=1
let g:coc_global_extensions = [ 'coc-json','coc-tsserver', 'coc-clangd', 'coc-java', 'coc-pyright', 'coc-lua', 'coc-angular', 'coc-html', 'coc-css', 'coc-pairs', 'coc-prettier', 'coc-snippets', 'coc-texlab', 'coc-go']

" diagnostics
nmap <Leader>gb :CocDiagnostics<CR>

" organize Import
nmap <Leader>gi :CocCommand editor.action.organizeImport<CR>
nmap <Leader>gf :CocCommand editor.action.formatDocument<CR>

" Remap keys for gotos
nmap <Silent> gd <Plug>(coc-definition)
nmap <Silent> gy <Plug>(coc-type-definition)
nmap <Silent> gi <Plug>(coc-implementation)
nmap <Silent> gr <Plug>(coc-references)

" Symbol renaming
nmap <leader>rn <Plug>(coc-rename)

" Use tab for trigger completion with characters ahead and navigate
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

let g:coc_snippet_next = '<tab>'

" Add Format command to format current buffer
noremap <leader>f :call CocActionAsync('format')<CR>

" use <c-space>for trigger completion
inoremap <silent><expr> <c-space> coc#refresh()

" Use <Tab> and <S-Tab> to navigate the completion list
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
autocmd BufWritePre *.go :call CocAction('runCommand', 'editor.action.organizeImport')
