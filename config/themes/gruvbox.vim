" options: soft, medium, hard
let g:gruvbox_contrast_dark = "hard"
let g:gruvbox_contrast_light = "soft"

colorscheme gruvbox
